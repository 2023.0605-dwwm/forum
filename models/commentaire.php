<?php

include_once "contenu.php";

class Commentaire extends Contenu {
    
    private Contenu $sujet;

    public function getSujet(): Contenu {
        return $this->sujet;
    }

    public function setSujet(Contenu $sujet) {
        $this->sujet = $sujet;
    }
}

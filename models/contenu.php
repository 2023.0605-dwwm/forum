<?php

include_once "utilisateur.php";

abstract class Contenu {

    private int $id;
    private DateTime $date;
    private string $contenu;
    private Utilisateur $auteur;
    private $commentaires = [];

    public function getId(): int {
        return $this->id;
    }

    public function setId(int $id) {
        $this->id = $id;
    }

    public function getDate(): DateTime {
        return $this->date;
    }

    public function setDate(DateTime $date) {
        $this->date = $date;
    }

    public function getContenu(): string {
        return $this->contenu;
    }

    public function setContenu(string $contenu) {
        $this->contenu = $contenu;
    }

    public function getAuteur(): Utilisateur {
        return $this->auteur;
    }

    public function setAuteur(Utilisateur $auteur) {
        $this->auteur = $auteur;
    }

    public function getCommentaires() {
        return $this->commentaires;
    }
}


?>
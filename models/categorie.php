<?php

class Categorie {

    private int $id;
    private string $nom;
    private array $messages = [];

    public function getId(): int {
        return $this->id;
    }

    public function setId(int $id) {
        $this->id = $id;
    }

    public function getNom(): string {
        return $this->nom;
    }

    public function setNom(string $nom) {
        $this->nom = $nom;
    }

    public function getMessages(): array {
        return $this->messages;
    }
}

?>
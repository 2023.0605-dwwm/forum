<?php

include_once "contenu.php";
include_once "categorie.php";

class Message extends Contenu {

    private string $titre;
    private Categorie $categorie;

    public function getTitre(): string {
        return $this->titre;
    }

    public function setTitre(string $titre) {
        $this->titre = $titre;
    }

    public function getCategorie(): Categorie {
        return $this->categorie;
    }

    public function setCategorie(Categorie $categorie) {
        $this->categorie = $categorie;
    }
}

?>
<?php

include_once "roleUtilisateur.php";
include_once "contenu.php";

class Utilisateur
{

    private int $id;
    private string $nom;
    private string $motDePasse;
    private RoleUtilisateur $role;
    private array $contenus = [];



    /**
     * Get the value of id
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id) {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of nom
     */
    public function getNom() {
        return $this->nom;
    }

    /**
     * Set the value of nom
     *
     * @return  self
     */
    public function setNom($nom) {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get the value of motDePasse
     */
    public function getMotDePasse() {
        return $this->motDePasse;
    }

    /**
     * Set the value of motDePasse
     *
     * @return  self
     */
    public function setMotDePasse($motDePasse) {
        $this->motDePasse = $motDePasse;

        return $this;
    }

    /**
     * Get the value of role
     */
    public function getRole(): RoleUtilisateur {
        return $this->role;
    }

    /**
     * Set the value of role
     *
     * @return  self
     */
    public function setRole(RoleUtilisateur $role) {
        $this->role = $role;

        return $this;
    }

    /**
     * Get the value of contenus
     */
    public function getContenus() {
        return $this->contenus;
    }

    /**
     * Set the value of contenus
     *
     * @return  self
     */
    public function setContenus($contenus) {
        $this->contenus = $contenus;

        return $this;
    }
}

DROP DATABASE IF EXISTS forum;
CREATE DATABASE forum;
USE forum;

CREATE TABLE categorie (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(255)
);

CREATE TABLE utilisateur (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(255),
    motDePasse VARCHAR(255),
    role VARCHAR(255)
);

CREATE TABLE contenu (
    id  BIGINT AUTO_INCREMENT PRIMARY KEY,
    dtype VARCHAR(255),
    date DATE,
    contenu TEXT,
    titre VARCHAR(255),
    idAuteur BIGINT,
    idCategorie BIGINT,
    idSujet BIGINT,
    FOREIGN KEY (idAuteur) REFERENCES utilisateur(id),
    FOREIGN KEY (idCategorie) REFERENCES categorie(id),
    FOREIGN KEY (idSujet) REFERENCES contenu(id)
);
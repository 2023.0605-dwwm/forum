<?php

// include_once "connect.php";
include_once "models/utilisateur.php";
include_once "config.php";

class UtilisateurRepository {

    public function findAll(): array {
        // etablir une connxion avec la DB
        $connexion = new PDO("mysql:dbname=forum;host=".DATABASE_URL, DATABASE_USER, DATABASE_PWD);
        // faire la requete
        $rows = $connexion->query("SELECT * FROM utilisateur");
        // extraire les resultats et construire un tableau d'objets Utilisateur
        $utilisateurs = [];
        foreach ($rows as $row) {
            $utilisateur = new Utilisateur();
            $utilisateur->setId($row['id']);
            $utilisateur->setNom($row['nom']);
            $utilisateur->setMotDePasse($row['motDePasse']);
            $utilisateur->setRole(RoleUtilisateur::from($row['role']));
            $utilisateurs[] = $utilisateur;
        }
        return $utilisateurs;
    }

    public function findById(int $id): Utilisateur {
        // etablir une connxion avec la DB
        $connexion = new PDO("mysql:dbname=forum;host=".DATABASE_URL, DATABASE_USER, DATABASE_PWD);
        // faire la requete
        $query = $connexion->prepare("SELECT * FROM utilisateur WHERE id = :id");
        $query->bindValue("id", $id);
        $row = $query->fetch();
        // extraire les resultats et construire un tableau d'objets Utilisateur
        $utilisateur = new Utilisateur();
        $utilisateur->setId($row['id']);
        $utilisateur->setNom($row['nom']);
        $utilisateur->setMotDePasse($row['motDePasse']);
        $utilisateur->setRole(RoleUtilisateur::from($row['role']));
        return $utilisateur;
    }

    public function save(Utilisateur $utilisateur): Utilisateur {
        // etablir une connxion avec la DB
        $connexion = new PDO("mysql:dbname=forum;host=".DATABASE_URL, DATABASE_USER, DATABASE_PWD);
        // faire la requete
        $query = $connexion->prepare("INSERT INTO utilisateur (nom, motDePasse, role) VALUE (:nom, :motDePasse, :role)");
        $query->bindValue("nom", $utilisateur->getNom());
        $query->bindValue("motDePasse", $utilisateur->getMotDePasse());
        $query->bindValue("role", $utilisateur->getRole()->value);
        $query->execute();
        return $utilisateur;
    }

    public function update(Utilisateur $utilisateur): void {
        // etablir une connxion avec la DB
        $connexion = new PDO("mysql:dbname=forum;host=".DATABASE_URL, DATABASE_USER, DATABASE_PWD);
        // faire la requete
        $query = $connexion->prepare("UPDATE utilisateur SET nom=:nom, motDePasse=:motDePasse role=:role WHERE id=:id");
        $query->bindValue("nom", $utilisateur->getNom());
        $query->bindValue("id", $utilisateur->getId());
        $query->bindValue("motDePasse", $utilisateur->getMotDePasse());
        $query->bindValue("role", $utilisateur->getRole()->value);
        $row = $query->execute();
    }

    public function delete(Utilisateur $utilisateur): void {
        $this->deleteById($utilisateur->getId());
    }

    public function deleteById(int $id): void {
        // etablir une connxion avec la DB
        $connexion = new PDO("mysql:dbname=forum;host=".DATABASE_URL, DATABASE_USER, DATABASE_PWD);
        // faire la requete
        $query = $connexion->prepare("DELETE FROM utilisateur WHERE id=:id");
        $query->bindValue("id", $id);
        $row = $query->execute();
    }
}

?>
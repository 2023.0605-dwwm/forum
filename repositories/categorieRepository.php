<?php

// include_once "connect.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/forum/models/categorie.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/forum/config.php";

class CategorieRepository {

    public function findAll(): array {
        // etablir une connxion avec la DB
        $connexion = new PDO("mysql:dbname=forum;host=".DATABASE_URL, DATABASE_USER, DATABASE_PWD);
        // faire la requete
        $rows = $connexion->query("SELECT * FROM categorie c");
        // extraire les resultats et construire un tableau d'objets Categorie
        $categories = [];
        foreach ($rows as $row) {
            $categorie = new Categorie();
            $categorie->setId($row['id']);
            $categorie->setNom($row['nom']);
            $categories[] = $categorie;
        }
        return $categories;
    }

    public function findById(int $id): Categorie {
        // etablir une connxion avec la DB
        $connexion = new PDO("mysql:dbname=forum;host=".DATABASE_URL, DATABASE_USER, DATABASE_PWD);
        // faire la requete
        $query = $connexion->prepare("SELECT * FROM categorie c WHERE c.id = :id");
        $query->bindValue("id", $id);
        $query->execute();
        $row = $query->fetch();
        // extraire les resultats et construire un tableau d'objets Categorie
        $categorie = new Categorie();
        $categorie->setId($row['id']);
        $categorie->setNom($row['nom']);
        return $categorie;
    }

    public function save(Categorie $categorie): Categorie {
        // etablir une connxion avec la DB
        $connexion = new PDO("mysql:dbname=forum;host=".DATABASE_URL, DATABASE_USER, DATABASE_PWD);
        // faire la requete
        $query = $connexion->prepare("INSERT INTO categorie (nom) VALUE (:nom)");
        $query->bindValue("nom", $categorie->getNom());
        $row = $query->execute();
        return $categorie;
    }

    public function update(Categorie $categorie): void {
        // etablir une connxion avec la DB
        $connexion = new PDO("mysql:dbname=forum;host=".DATABASE_URL, DATABASE_USER, DATABASE_PWD);
        // faire la requete
        $query = $connexion->prepare("UPDATE categorie SET nom=:nom WHERE id=:id");
        $query->bindValue("nom", $categorie->getNom());
        $query->bindValue("id", $categorie->getId());
        $row = $query->execute();
    }

    public function delete(Categorie $categorie): void {
        $this->deleteById($categorie->getId());
    }

    public function deleteById(int $id): void {
        // etablir une connxion avec la DB
        $connexion = new PDO("mysql:dbname=forum;host=".DATABASE_URL, DATABASE_USER, DATABASE_PWD);
        // faire la requete
        $query = $connexion->prepare("DELETE FROM categorie WHERE id=:id");
        $query->bindValue("id", $id);
        $row = $query->execute();
    }
}

?>
## Repositories

1. créer un fichier `repositories/utilisateurRepository.php`
2. copier-coller le contenu de `categorieRepository.php` dans `utilisateurRepository.php`
3. dans `utilisateurRepository.php`
    - remplacer categorie par utilisateur (type et nom de variables, nom de table)
    - modifier la conversion de Utilisateur => SQL (dans save et update)
    - modifier la conversion de SQL => Utilisateur (dans findAll et findById)

## Page de détail des catégories

1. Dans la vue `categoriesList.php`, ajouter pour chaque categorie un line vers l'url `categories/details?id=<id-de-la-categorie>`
2. Créer un controller `controllers/categories/details/index.php` qui :
    - inclure et créer (new) un `categorieRepository`
    - recuperer l'id dans les parametres de requetes (isset ... $GET["id"] ...)
    - aller chercher la categorie dans la base de données (`findById(...)`) et la mettre dans un evariable `$categorie`
    - include la vue `../../views/categoriesDetail.php`
3. Créer une vue `views/categoriesDetail.php` qui
    - affiche la categorie `$categorie`
    - un lien pour revenir à la liste des vues
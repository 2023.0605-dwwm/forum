# Forum

## Description du besoin

Les utilisateurs connectés peuvent poster des messages. Un message est constitué d'un titre, d'une catégorie et d'un contenu. La date et l'auteur du message seront également sauvegardés dans le base de données.

Les visiteurs pourront voir les messages postés. Les utilisateurs pourront également commentéer les messages (ou répondre aux commentaires).

Un administrateur ou l'auteur d'un message (ou d'un commentaire) pourront éditer ou supprimer celui-ci.

Les messages seront organisés par catégories. seuls les administrateurs pourront créer, modifier ou supprimer des catégories.

## Étapes à suivre

1. Faire le diagramme de cas d'utilisation
    - identifier les acteurs
    - identifier les cas d'utilisation
    - relier tout ça
    - factoriser (si pertinent) avec de l'héritage
2. Faire le diagramme de classe
    - identifier les classes
    - relier les classes (associations, agrégations, ...)
    - ajouter les attributs
    - factoriser avec de l'héritage
3. Coder (on le fera ensemble)
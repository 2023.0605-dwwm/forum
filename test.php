<?php

include_once "repositories/categorieRepository.php";
include_once "repositories/utilisateurRepository.php";
include_once "models/roleUtilisateur.php";

// $categorieRepository = new CategorieRepository();

// // $cat = new Categorie();
// // $cat->setNom("programmation"); 

// // $categorieRepository->save($cat);

// $categories = $categorieRepository->findAll();

// foreach ($categories as $cat)
//     print($cat->getId() . " " . $cat->getNom() . "\n");


$utilisateurRepository = new UtilisateurRepository();
// save
$utilisateur = new Utilisateur();
$utilisateur->setNom("andre");
$utilisateur->setMotDePasse("erdna");
$utilisateur->setRole(RoleUtilisateur::UTILISATEUR);
$utilisateurRepository->save($utilisateur);
// find all
$utilisateurs = $utilisateurRepository->findAll();
foreach ($utilisateurs as $utilisateur)
    print("\tid=" . $utilisateur->getId() . " nom=" . $utilisateur->getNom() . " role=" . $utilisateur->getRole()->value . "\n");

?>
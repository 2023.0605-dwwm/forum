<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Forum</title>
</head>
<body>

<h1><?php echo isset($categorie) ? "Modification de la catégorie ".$categorie->getId() : "Nouvelle catégorie"; ?></h1>

<form method="post">
    <input type="text" name="nom" placeholder="Nom" value='<?php echo isset($categorie) ? $categorie->getNom() : "" ?>'/>
    <a href="../categories.php">annuler</a>
    <button type="submit">enregistrer</button>
</form>

</body>
</html>
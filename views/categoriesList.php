<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Forum</title>
</head>

<body>

    <h1>Catégories</h1>

    <a href='categories/add.php'>ajouter</a>

    <ul>
        <?php foreach ($categories as $cat) { ?>
            <li>
                <?php echo $cat->getId() . " - " . $cat->getNom() ?>
                <a href='categories/details.php?id=<?php echo $cat->getId(); ?>'>detail</a>
                <a href='categories/edit.php?id= <?php echo $cat->getId(); ?>'>modifier</a>
                <a href='categories/delete.php?id= <?php echo $cat->getId(); ?>'>supprimer</a>
            </li>
        <?php } ?>
    </ul>
</body>

</html>
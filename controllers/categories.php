<?php

    include_once "../repositories/categorieRepository.php";

    $categorieRepository = new CategorieRepository();

    // recuperation des infos de la requete
    // interaction avec le model (service, repositories)
    $categories = $categorieRepository->findAll();
    // generation de la reponse (avec les vues)
    include "../views/categoriesList.php"

?>
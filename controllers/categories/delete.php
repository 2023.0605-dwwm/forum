<?php

    include_once "../../repositories/categorieRepository.php";

    $categorieRepository = new CategorieRepository();

    // recuperation des infos de la requete
    $id = $_REQUEST["id"];
    // interaction avec le model (service, repositories)
    $categorie = $categorieRepository->deleteById($id);
    // generation de la reponse (avec les vues)
    header('Location: ../categories.php');
    die();
?>
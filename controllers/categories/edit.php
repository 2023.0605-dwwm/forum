<?php

    include_once "../../repositories/categorieRepository.php";

    $categorieRepository = new CategorieRepository();

    // si méthode GET
    if ($_SERVER['REQUEST_METHOD'] == "GET") {
        // recuperation des infos de la requete
        $id = $_REQUEST["id"];
        // interaction avec le model (service, repositories)
        $categorie = $categorieRepository->findById($id);
        // generation de la reponse (avec les vues)
        include "../../views/categoriesForm.php";
    }
    // si méthode POST
    else if ($_SERVER['REQUEST_METHOD'] == "POST") {
        // recuperation des infos de la requete
        $categorie = new Categorie();
        $categorie->setId($_REQUEST["id"]);
        $categorie->setNom($_REQUEST["nom"]);
        // interaction avec le model (service, repositories)
        $categorie = $categorieRepository->update($categorie);
        // generation de la reponse (avec les vues)
        header('Location: ../categories.php');
        die();
    }
?>
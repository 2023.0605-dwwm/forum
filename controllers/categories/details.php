<?php

    include_once "../../repositories/categorieRepository.php";

    $categorieRepository = new CategorieRepository();

    // recuperation des infos de la requete
    $id = $_REQUEST["id"];
    // interaction avec le model (service, repositories)
    $categorie = $categorieRepository->findById($id);
    // generation de la reponse (avec les vues)
    include "../../views/categoriesDetails.php"

?>
<?php

    include_once "../../repositories/categorieRepository.php";

    $categorieRepository = new CategorieRepository();

    // recuperation des infos de la requete
    // interaction avec le model (service, repositories)
    $categories = $categorieRepository->findAll();
    // generation de la reponse (avec les vues)
    foreach ($categories as $cat)
        echo $cat->getId() . " " . $cat->getNom() . "\n";

?>